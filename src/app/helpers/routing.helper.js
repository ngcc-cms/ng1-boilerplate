import { StringHelper } from './string.helper';
const _ = new StringHelper();

export class RoutingHelper {
  constructor() {
    this._ = _;
  }

  goto(url, newTab = false) {
    if (!newTab) {
      window.location.replace(url);
    }

    window.open(url, true);
  }

  changeUrl(url) {
    // console.log('URL Changed: ', url)
    window.history.pushState('', '', url);
  }

  onRouteChange() {
    const pushState = history.pushState;
    history.pushState = (e, i) => {
      // pushState.apply(history, e, i);
      // return 'test';
    };
  }
  
  get urlParams() {
    const urlParams = window.location.search.replace('?', '').split('&');
    const params = {};
    urlParams.forEach(urlParam => {
      const param = urlParam.split('=');
      params[param[0]] = decodeURIComponent(param[1]).split(',');
    });

    return params;
  }

}