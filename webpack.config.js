const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var historyApiFallback = require('connect-history-api-fallback');

module.exports = {
    mode: 'development',
    entry: {
      main: './src/index.js',
    },
    output: {
      filename: '[name].js',
      publicPath: 'dist'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        {
          test: /\.(html)$/,
          use: {
            loader: 'html-loader',
            options: {
              attrs: [':data-src']
            }
          }
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
      ]
    },
    plugins: [
        new BrowserSyncPlugin({
          // ./public directory is being served
          watch: true,
          host: 'localhost',
          port: 3201,
          server: { 
            baseDir: ['src'],
            serveStaticOptions: { 
              extensions: ['html']
            },
            middleware: [ historyApiFallback()]
          },
        })
    ],
    watch: true
  };
  