import template from './input.text.html';

export class InputText {
  constructor(config) {
    this.template = template;
    this.config = config;
    this.$onInit();
  }

  configureTemplate() {
  }
  
  $onInit() {
  }

  $onChanges(changes) {

  }

  addListener() {

  }
}