import { StringHelper } from './string.helper';
const _ = new StringHelper();

export class ElementHelper {
  constructor() {
    this._ = _;
  }
  
  setText(text, $el) {
    if ($el && this.isElement($el)) {
      $el.innerText = text;
    }
  }

  isElement($el) {
    return this.isDOMElement($el) || this.isNode($el);
  }

  isDOMElement(el) {
    return typeof HTMLElement === "object"
      ? el instanceof HTMLElement
      : el && typeof el === "object" && el !== null && el.nodeType === 1 && typeof el.nodeName==="string";
  }

  isNode(node) {
    return typeof Node === "object"
      ? node instanceof Node
      : node && typeof node === "object" && typeof node.nodeType === "number" && typeof node.nodeName==="string";
  }

  find($el) {
    return document.querySelector($el);
  }

  /**
   * @param {*} $el element to find by class or id
   * @returns sets of element
   */
  findAll($el) {
    return document.querySelectorAll($el);
  }

  create($el) {
    return document.createElement($el);
  }

  set($el, type, val) {
    const elAttr = $el[type] = val;
    return elAttr;
  }

  addClass($el, classes) {
    if (!classes && !this.isElement($el)) {
      console.error(`Failed to add class ${$el}, element not defined.`);
      return 0;
    } 
    if (!this.isElement($el) && classes) {
      console.error(`Failed to add class ${$el}, cannot find element ${$el}.`)
      return 0;
    }
    return $el.classList.add(classes);

  }

  removeClass($el, classes) {
    if (!classes && !this.isElement($el)) {
      console.error(`Failed to remove class ${$el}, element not defined.`);
      return 0;
    } 
    if (!this.isElement($el) && classes) {
      console.error(`Failed to remove class ${$el}, cannot find element ${$el}.`)
      return 0;
    }

    return $el.classList.remove(classes);
  }

  html(el, find, findAll = false) {
    const $el = new DOMParser().parseFromString(el, 'text/html').body;
    if (find) {
      if (_.isNumber(find)) {
        return $el.children[find];
      } else if (find === 'first') {
        return $el.firstChild;
      } else if (_.isString(find)) {
        return $el.querySelector(find);
      } else if (_.isString(find) && findAll) {
        return $el.querySelectorAll(find);
      }
    }

    return $el.firstChild;
  }

  convert(el) {

  }

  getElementType(type) {
    const inputs = [
      'radio',
      'checkbox',
      'text',
      'number',
      'date',
      'search',
      'submit',
      'url',
      'hidden'
    ];

    const dropdowns = [
      'select',
      'dropdown',
    ]

    if (inputs.includes(type)) {
      return 'input';
    }

    if (dropdowns.includes(type)) {
      return 'select';
    }

    return type;
  }
}