import { InputText } from './inputs/text/input.text';
import { tmpl_container } from './containers/containers';

export {
    InputText,
    tmpl_container,
}