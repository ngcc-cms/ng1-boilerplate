const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const uglify = require('gulp-uglify');
const htmlMinify = require('gulp-htmlmin');
const rename = require('gulp-rename');
const webpack = require('webpack-stream');
const srcPath = `src/app`;

/**
* @description compiles scss files in 'src/app/scss'
*/
gulp.task('sass:main', () => {
  return gulp
  .src(`${srcPath}/scss/**/*.scss`)
  .pipe(sass())
  .on('error', function(err) {
    console.log(err.toString());
    this.emit('end');
  })
  .pipe(gulp.dest(`${srcPath}/css`))
  .pipe(browserSync.reload({
    stream: true
  }))
})

/**
* @description watches for `scss` file and trigger hot reload
*/
gulp.task('sass:watch', () => {
  gulp.watch(`${srcPath}/scss/**/*.scss`, ['sass:main'])
});

/**
* @description hot reload when html files saved
*/
gulp.task('browserSync', () => {
  browserSync.init({
    server: {
      baseDir: 'app',
      serveStaticOptions: {
        extensions: ['html']
      },
    }
  })
});

/**
* @description transpiles `index.js` to /dist
*/
gulp.task('webpack', () => {
  return gulp.src(`src/index.js`)
  .pipe(webpack( require('./webpack.config.js')))
  .pipe(gulp.dest(`src/dist`));
});

/**
* @description minifies js files
*/
gulp.task('webpack:uglify', () => {
  return gulp
  .src(`src/dist/**/*.js`)
  .pipe(uglify())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest(`src/dist/js/min`))
})

/**
* Manually call this tasks
*/

/**
* @description minifies js files
*/
gulp.task('uglify', () => {
  return gulp
  .src(`${srcPath}/js/**/*.js`)
  .pipe(uglify())
  .pipe(gulp.dest(`src/dist/js`))
})

/**
* @description minifies js files
*/
gulp.task('htmlMinify', () => {
  return gulp
  .src(`${srcPath}/**/*.html`)
  .pipe(htmlMinify({
    collapseWhitespace: true
  }))
  .pipe(gulp.dest(`src/dist/html`));
})

/**
* @description run task 'gulp' - contains hot reload for `sass, html, js` files
*/
gulp.task('default', ['sass:main', 'sass:watch', 'webpack'], () => {
  gulp.watch(`${srcPath}/**/*.html`, browserSync.reload)
  gulp.watch(`${srcPath}/js/**/*.js`, browserSync.reload)
});