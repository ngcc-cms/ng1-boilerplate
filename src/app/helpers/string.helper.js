export class StringHelper {
  constructor() {}

  clone(obj) {
    if (obj) {
      return JSON.parse(JSON.stringify(obj));
    }

    return obj;
  }

  cloneDeep(obj) {
    const returnLiteral = [
      undefined,
      null,
      0,
    ];
    if (typeof obj === 'string') {
      return obj.toString();
    } else if (typeof obj === 'number') {
      return Number.parseInt(obj);
    } else if (returnLiteral.includes(obj)) {
      return obj;
    }

    return this.isObject(obj) ? Object.assign({}, obj) : this.clone(obj);
  }

  isArray(arr) {
    return Array.isArray(arr);
  }

  isObject(obj) {
    return typeof obj === 'object' && !Array.isArray(obj);
  }

  isString(str) {
    return typeof str === 'string';
  }

  isNumber(n) {
    return /^\d+$/g.test(n);
  }

  toNumber(s) {
    return isNaN(parseInt(s)) ? s : parseInt(s);
  }

  toUrlParam(obj) {
    return new URLSearchParams(obj).toString();
  }

  stringify(obj) {
    return JSON.stringify(obj);
  }

  hasDuplicate(arrs) {
    if (!this.isArray(arrs)) {
      console.error(`${arrs} is not a valid Array of type`);
      return 0;
    }

    return arrs.every((el, index, arr) => el === arr[0]);
  }

  /**
   * 
   * @param {Array} arr - supports array for now
   * @param {string} find - what to find inside arr
   */
  contains(arr, find) {
    return arr.includes(find);
  }

  hasKeys(obj) {
    return this.isObject(obj) ? Object.keys(obj).length > 0 : false;
  }

  keys(obj) {
    return Object.keys(obj);
  }
}