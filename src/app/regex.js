export class Regex {

    email(str) {
        if(!/[\w\d]+@+[\w\d]+[.]+[\w]/gm.test(str)) {
            return {
                isValid: false,
                message: 'Invalid email format'
            }
        }

        return {
            isValid: true,
            message: 'Email is invalid'
        }
    }

    password(str) {
        if(!/[!@#$%^&*()]/gm.test(str)) {
            return {
                isValid: false,
                message: 'String must contain special character'
            };
        }
        
        if(/[0-9]/gm.test(str)) {
            return {
                isValid: false,
                message: 'String must contain number'
            };
        }

        return {
            isValid: true,
            message: 'Password is valid'
        };
    }
}