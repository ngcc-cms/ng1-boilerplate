import { app } from './app/app';
const form = [
  {
    type: 'radio',
    name: 'radio_test',
    value: 'r1',
    classes: '',
    label: {
      text: 'Gender',
      classes: 'test mic',
    },
    options: [
      {
        text: 'Radio 1',
        value: 'radio1'
      },
      {
        text: 'Radio 2',
        value: 'radio2',
        selected: true,
      }
    ],
    group: 1
  },
  {
    type: 'checkbox',
    name: 'checkbox',
    classes: '',
    label: {
      text: 'Checkbox',
    },
    options: [
      {
        text: 'checkbox 1',
        value: 'cb1',
        selected: true,
      },
      {
        text: 'checkbox 2',
        value: 'cb2',
      }
    ],
    group: 1
  },
  {
    type: 'number',
    name: 'name',
    placeholder: 'Enter Name',
    label: {
      text: 'Name',
      classes: '',
    },
    group: 2,
    depends: [
      {
        lvalue: 'dropdown_1',
        comparator: 'eq',
        rvalue: 'test2'
      }
    ]
  },
  {
    type: 'dropdown',
    name: 'dropdown_1',
    label: {
      text: 'Dropdown 1',
    },
    options: [
      {
        text: 'test 1',
        value: 'test1'
      },
      {
        text: 'test 2',
        value: 'test2'
      },
      {
        text: 'test 3',
        value: 'test3',
        selected: true,
      }
    ],
    group: 3,
  }
];

const config = {
  // group_class: 'inline-group',
  group_class: 'block-group',
  label: {
    position: 'left'
  },
  default_classes: true,
  depends_hidden_classes: ['acf-field-hidden'],
  depends_class: {
    hidden: ['acf-field-hidden'],
    destroyDepends: true
  },
  form: {
    exclude_hidden_field_param: true,
    action: '/',
    method: 'GET',
  },
  enable_logs: false,
}


const myForm = app('.test').init(
  form,
  config,
  true,
);

myForm.on('submit', (e) => {
  console.log('params: ', e)
});

myForm.on('changes', (changes) => {
  console.log('changes: ', changes)
});

// console.log('myForm: ', myForm.on('submit'))
// console.log('myform: ', myForm.on('submit', (params) => {
//   console.log('[params]: ', params)
// }))


// console.log('myForm: ', myForm)