import { $, _, $route } from './helpers/helpers';
import { InputText, tmpl_container } from './templates/templates';

class FormControl {
  constructor(
    $el
  ) {
    // this.$ = $;
    // this._ = _;
    // this.$route = $route;
    this.formConfig = {};
    this.formGroups = {}; // comment out
    this.$form; // comment out
    this.changes = {};
    this.eventTypes = [];
    this.callbackFns = {};
    // this.oldValues = {};
    this.forms = [];
    this.$el = $.find($el);
  }

  init(forms, formConfig, overrideMissing = false) {
    if (!forms || !_.isArray(forms)) {
      console.error(`Error: Form should be in array`);
    }
    if (_.isArray(formConfig) || typeof formConfig !== 'object') {
      console.error(`Error: Config should be object received array`);
      return;
    }
    this.forms = forms;
    if (overrideMissing) {
      this.overrideConfig(formConfig);
    }
    this.formConfig = _.hasKeys(formConfig) ? formConfig : this.defaultFormConfig();

    this.arrangeGroups();
    this.buildFormGroup();
    console.log('Form loaded! ', {
      config: _.cloneDeep(this.formConfig),
    });

    $route.onRouteChange();

    return this;

  }

  onUrlChange() {

  }

  overrideConfig(formConfig) {
    _.keys(this.defaultFormConfig()).forEach(config => {
      if (formConfig[config] === undefined) {
        formConfig[config] = this.defaultFormConfig()[config];
      }

      if (_.isObject(this.defaultFormConfig()[config])) {
        _.keys(this.defaultFormConfig()[config]).forEach(conf => {
          if (formConfig[config][conf] === undefined) {
            formConfig[config][conf] = this.defaultFormConfig()[config][conf];
          }
        })
      }
    });
  }

  defaultFormConfig() {
    return {
      group_class: 'inline-group',
      label: {
        position: 'left',
      },
      override: true,
      depends_class: {
        hidden: ['acf-field-hidden'],
        destroyDepends: false,
      },
      form: {
        exclude_hidden_field_param: true,
        submit_button: {
          text: 'Submit'
        }
      }
    };
  }

  getInputEl(field, find, findAll) {
    if (field?.type) {
      switch (field.type) {
        case 'text':
        case 'number':
          return $.html(new InputText(field).template, find, findAll);
        case 'radio':
          return '';
        default:
          return $.html(new InputText(field).template, find, findAll);
      }
    }

    return console.log(`Unable to create field: ${_.clone(field)}. Missing properties.`);
  }

  configureForm() {
    const { form } = this.formConfig;
    const $form = $.create('form');

    const formAttrs = [
      'action',
      'method'
    ];

    formAttrs.forEach(attr => {
      $form[attr] = form[attr];
      if (attr === 'action' && form[attr] === 'self') {
        $form.removeAttribute(attr);
      }
    });
    return $form;
  }

  generateFieldId(field, hasSameName, index) {
    return `${field?.name || field?.label?.text}${hasSameName ? `-${index}` : ''}`.replace(' ', '-');
  }


  createLabel(field, isValidGroup = false) {
    if (!field.label) {
      return;
    }
    const $label = $.create('label');
    this.setLabelAttrs($label, field, isValidGroup);
    return $label;
  }

  buildFormGroup() {
    // this.$form;
    // this.$formGroups = [];
    const $formContainer = this.configureForm();
    const formGroupKeys = Object.keys(this.formGroups);
    const labelConfig = this.formConfig.label;
    if (formGroupKeys.length > 0) {
      formGroupKeys.forEach(formGroupKey => {
        const formGroup = this.formGroups[formGroupKey];
        const $formGroupContainer = $.create('div');
        $.addClass($formGroupContainer, 'acf-form-group');
        const isValidGroup = formGroup.length > 1;
        let hasSameName = false;
        if (isValidGroup) {
          hasSameName = _.hasDuplicate(formGroup.map(field => field?.label?.text));
        }

        // Add class form-group-#
        $.addClass($formGroupContainer, `form-group-${formGroupKey}`)
        // Set groups class inline/block group
        // if (isValidGroup) {
          $.addClass($formGroupContainer, this.formConfig.group_class);
        // }

        const typeWithOptions = [
          'radio',
          'dropdown',
          'checkbox'
        ];

        formGroup.forEach((field, fieldIndex) => {
          const $container = $.create('div');
          const $el = $.create($.getElementType(field?.type));
          const $label = this.createLabel(field, isValidGroup);
          const { position } = labelConfig;
          field.id = this.generateFieldId(field, hasSameName, fieldIndex);
          // container class
          $.addClass($container, `acf-${field?.type}`);
          $.addClass($container, `acf-form-field`);
          if ($el) {
            if (!typeWithOptions.includes(field.type)) {
              if (isValidGroup) {
                $.set($el, 'name', `group-${formGroupKey}`);
                // field.name = `group-${formGroupKey}`;
              }
              this.setElAttrs($el, field, isValidGroup);
              if (position === 'right') {
                $container?.append($el);
                if ($label) {
                  $container?.append($label);
                }
              } else {
                if ($label) {
                  $container?.append($label);
                }
                $container?.append($el);
              }

              if (!$el.value) {
                $el.value = '';
              }

              // Attach event listener
              this.addEvents($el, field);
            } else if (typeWithOptions.includes(field.type)) {
              if (position === 'right') {
                // $container?.append($el);
                if ($label) {
                  $container?.append($label);
                }
              } else {
                if ($label) {
                  $container?.append($label);
                }
                // $container?.append($el);
              }
              // set options
              if (field?.options) {
                this.addOptions($el, $container, field);
                if (field.type === 'dropdown') {
                  this.addEvents($el, field);
                }
              }

            }
          }

          $formGroupContainer.append($container);
          // this.$formGroups.push($container);
        });

        $formContainer?.append($formGroupContainer);
      });

      const $button = $.create('button');
      $button.innerText = this.formConfig.form?.submit_button?.text;
      $button.type = 'submit';

      $formContainer.append($button);
      $formContainer.addEventListener('submit', (e) => {
        e.preventDefault();
        // const changes = this.getRawValues();

        // emit on changes
        this.onChanges(this.getRawValues(), true, true);
        // this.setFinalValues(changes);
        // console.log('URL: ', $route.urlParams);
        // $route.goto(`/?${_.toUrlParam(changes)}`);
        // $route.changeUrl(`/?${_.toUrlParam(changes)}`);
        // console.log('submitted: ', this)
        this.on('submit', this.callbackFns.submit, true);
      });
    }

    // this.$form = $formContainer;
    this.$el.append($formContainer);
    // delete this.$el;

    // trigger first onChanges
    this.forms.forEach(formGroup => {
      this.onChanges({}, formGroup);
    });

    // required to delete this.forms?
    delete this.forms;
  }

  getRawValues() {
    const newForm = this.getNewForm();
    const initialValues = {};
    newForm.forEach(form => {
      // field values[] or value
      const value = form.values || form.value;
      // check if field value is a valid
      const finalValue = _.isNumber(value) ? _.toNumber(value) : value;
      // exclude hidden param?
      if (this.formConfig.form?.exclude_hidden_field_param === false || form.show_field === true) {
        initialValues[form.name] = finalValue;
      }
      // field type 'hidden' automatically assigned in the form
      if (form.type === 'hidden') {
        initialValues[form.name] = finalValue;
      }
      // field values[] manipulation value
      if (form.values && _.isArray(form.values)) {
        if (form.values.length === 0) {
          delete initialValues[form.name];
        }
      }
      
    });

    return initialValues;
  }
  
  on(type, cb, isSubmitted) {
    if (!_.contains(this.eventTypes, type)) {
      this.eventTypes.push(type);
    }

    this.callbackFns[type] = cb;
    if (_.contains(this.eventTypes, type) && isSubmitted === true) {
      return this.callback(cb);
    }
  }

  callback(fn) {
    if (fn) {
      const callback = {
        $form: this.$el.querySelector('form'),
        fields: _.cloneDeep(this.$el.querySelector('form')),
        values: _.cloneDeep(this.getRawValues()),
      }

      return fn(callback);
    }
  }

  get values() {
    const finalValues = _.cloneDeep(this.finalValues);
    delete this.finalValues;
    return finalValues;
  }

  getNewForm() {
    const newForm = [];
    const forms = this.formGroups;
    _.keys(forms).forEach(form => {
      for (const field of forms[form]) {
        newForm.push(field);
      }
    });

    return newForm;
  }

  addOptions($el, $container, field) {
    if (_.isArray(field.options)) {
      field.options.forEach(option => {
        if (field.type === 'dropdown') {
          const $option = $.create('option');
          $option.value = option.value || option.text;
          $option.innerText = option.text || option.value;
          if (option.selected === true && !field.value) {
            $option.selected = true;
            $el.value = option.value;
            field.value = option.value;
          }

          this.setElAttrs($el, field, false);
          $el.append($option);
        } else if (field.type === 'radio' || field.type === 'checkbox') {
          const $wrapper = $.create('span');
          const $option = $.create($.getElementType(field?.type));
          const $label = $.create('label');
          $label.innerText = option.text;
          if (option.selected || field.value === option.value) {
            field.value = option.value;
            $option.checked = true;
            if (field.values) {
              field.values.push(option.value)
            }
          }
          this.setElAttrs($option, _.clone({...field, ...{option: { text: option.text, value: option.value}}}));
          this.setLabelAttrs($label, option);
          // $label.append($option);
          if ($label) {
            $wrapper.append($label);
          }

          $wrapper.append($option);
          this.addEvents($option, field);
          $container.append($wrapper);
        }
      });

      if (field.type === 'dropdown') {
        $container.append($el);
      }
    }
  }

  buildField(field) {
    
  }

  setElAttrs($el, field, isGroup) {
    field = _.cloneDeep(field);
    const attrs = [
      'value',
      'id',
      'name',
      'type',
      'data-name'
    ];

    const excludeAttts = {
      type: ['select', 'dropdown'],
    }

    const uniqId = [
      'radio',
      'checkbox'
    ]

    attrs.forEach(attr => {
      if (isGroup && attr === 'name') {
        attr = 'id';
      }

      if (uniqId.includes(field.type)) {
        field.id = field.option.text;
      }

      if (field.type === 'number') {
        field.type = 'text';
      }

      if (attr.includes('data-')) {
        $el.setAttribute(attr, field[attr.replace('data-', '')])
      }

      if (!excludeAttts[attr]?.includes(field.type)) {
        $.set($el, attr, field[`data-${attr}`] || field?.option && field?.option[attr] || field[attr]);
      }
    })
  }

  setLabelAttrs($label, field, isGroup) {
    if ($label.innerText === '') {
      $label.innerText = field?.label?.text;
    }

    $.set($label, 'htmlFor', field?.id || field?.text || field.value);
    $.set($label, 'class', field?.label?.classes);
  }

  addEvents($el, field) {
    const eventType = field?.event?.type || this.getEventType(field?.type);
    if (_.isArray(eventType)) {
      eventType.forEach($ev => {
        $el.addEventListener($ev, (e) => this.eventChanges(e, field));
      })
    } else {
      $el.addEventListener(eventType, function (e) { this.eventChanges(e, field) });
    }
  }

  eventChanges(e, field) {
    setTimeout(() => {
      let oldVal = _.cloneDeep(!_.hasKeys(this.changes[name]) ? null : this.changes[name]);
      let newVal = e.target.valueAsNumber || e.target.value;
      const name = e.target.name;
      if (field.type === 'number') {
        if (!_.isNumber(newVal)) {
          field.value = oldVal;
          e.target.value = field.value;
        } else if (_.isNumber(newVal)) {
          field.value = _.toNumber(newVal);
          e.target.value = field.value;
        }
      }
      let newChange = {};
      if (field.type === 'checkbox') {
        // const _o = _.cloneDeep(oldVal);
        // newVal = field.values;
        // if (_.isString(oldVal)) {
        //   oldVal = [];
        //   oldVal.push(_o);
        // }
      }
      const changes = {
        name: e.target?.dataset?.name,
        type: e.type,
        values: {
          current: newVal,
          old: oldVal ? oldVal : field.value || null,
        },
      }

      if (field.type === 'checkbox') {
        if (_.isArray(field.values)) {
          if (e.target.checked) {
            field.values.push(newVal);
          } else {
            field.values.splice(field.values.indexOf(newVal), 1);
          }
        }
      }

      field.value = newVal ? newVal : '';

      if (_.hasKeys(this.changes[name])) {
        _.keys(this.changes[name]).forEach(key => {
            newChange[key] = changes[key];
        });
      }

      // this.oldValue(changes.name, newVal);
      this.changes[name] = _.cloneDeep(changes);
      this.onChanges((_.hasKeys(newChange) ? newChange : changes), field, false);
    });
  }

  oldValue(name, value) {
    this.oldValues[name] = value;
  }

  onChanges(changes, field, isFormSubmit) {
    // logs changes
    if (_.hasKeys(changes) && this.enableLogs()) {
      console.log('changes: ', changes);
    }
    if (field) {
      const dependentFields = this.getDependentFields(field);
      this.fieldDisplay(field, dependentFields);
    }

    if (!isFormSubmit) {
      this.on('changes', this.callbackFns.changes, true);
    }
  }

  getDependentFields(field) {
    return this.getNewForm().filter(f => f.depends?.some(depend => depend.lvalue === field.name));
  }

  fieldDisplay(field, dependentFields) {
    dependentFields.forEach(dependentField => {
      dependentField.show_field = false;
      dependentField?.depends?.forEach(depend => {
        if (depend.comparator === 'eq') {
          if (field.value === depend.rvalue) {
            dependentField.show_field = true;
          }
        } else if (depend.comparator === 'ne') {
          if (field.value !== depend.rvalue) {
            dependentField.show_field = true;
          }
        } else if (depend.comparator === 'gte') {
          if (_.toNumber(field.value) >= _.toNumber(depend.rvalue)) {
            dependentField.show_field = true;
          }
        } else if (depend.comparator === 'gt') {
          if (_.toNumber(field.value) > _.toNumber(depend.rvalue)) {
            dependentField.show_field = true;
          }
        } else if (depend.comparator === 'lte') {
          if (_.toNumber(field.value) <= _.toNumber(depend.rvalue)) {
            dependentField.show_field = true;
          }
        } else if (depend.comparator === 'lt') {
          if (_.toNumber(field.value) < _.toNumber(depend.rvalue)) {
            dependentField.show_field = true;
          }
        }
      });

      this.showHideFields(dependentField);
    });
  }

  enableLogs() {
    return this.formConfig.enable_logs === true;
  }

  showHideFields(field) {
    if (this.enableLogs()) {
      console.log(`Field "${field.name}" is ${field.show_field ? 'visible' : 'hidden'}.`);
    }
    const DOMField = document.querySelector(`[data-name="${field.name}"]`);
    const fieldContainer = DOMField?.parentElement;
    if (DOMField && fieldContainer) {
      const hiddenClasses = this.formConfig?.depends_class?.hidden;
      const visibleClasses = this.formConfig?.depends_class?.visible;
      const destroyDepends = this.formConfig?.depends_class?.destroyDepends;

      // hide / destroy field
      if (!field.show_field) {
        if (!destroyDepends) {
          if (_.isArray(hiddenClasses)) {
            hiddenClasses.forEach(hiddenClass => {
              $.addClass(fieldContainer, hiddenClass);
            });
          } else if (_.isString(hiddenClasses)) {
            $.addClass(fieldContainer, hiddenClasses)
          } else {
            $.addClass(fieldContainer, hiddenClasses)
          }
  
          // remove visible classes
          if (_.isArray(visibleClasses)) {
            visibleClasses.forEach(visibleClass => {
              $.removeClass(fieldContainer, visibleClass);
            });
          } else if (_.isString(visibleClasses)) {
            $.removeClass(fieldContainer, visibleClasses);
          }
        } else if (destroyDepends === true) {
          // destroy fields
          $.addClass(fieldContainer, 'acf-field-display');
        }

      } else {
        // show / rebuild field
        if (!destroyDepends) {
          if (_.isArray(visibleClasses)) {
            visibleClasses.forEach(visibleClass => {
              $.addClass(fieldContainer, visibleClass);
            });
          } else if (_.isString(visibleClasses)) {
            $.addClass(fieldContainer, visibleClasses);
          }

          // remove hidden classes
          if (_.isArray(hiddenClasses)) {
            hiddenClasses.forEach(hiddenClass => {
              $.removeClass(fieldContainer, hiddenClass);
            });
          } else if (_.isString(hiddenClasses)) {
            $.removeClass(fieldContainer, hiddenClasses)
          } else {
            $.removeClass(fieldContainer, hiddenClasses)
          }
        } else if (destroyDepends === true) {
          // destroy fields
          $.removeClass(fieldContainer, 'acf-field-display');
        }
      }
    }
  }

  rebuildField(field) {
    console.log('rebuild: ', field)
  }

  getEventType(type) {
    const change = [
      'radio',
      'checkbox',
      'date',
      'dropdown',
      'select'
    ];

    const keyup = [
      'text',
      'number',
      'date',
      'search',
      'url',
    ];

    const click = [
      'submit',
    ];

    if (change.includes(type)) {
      return ['change', 'mouseclick', ''];
    } else if (keyup.includes(type)) {
      return ['keydown'];
    } else if (click.includes(type)) {
      return 'click';
    }
  }

  dependencies() {

  }

  arrangeGroups() {
    const forms = this.forms;
    forms.forEach(form => {
      form.show_field = form?.depends ? false : true;
      form.attrs = {};
      form.attrs.name = form.name;
      form.value = form.value ? form.value : _.isNumber(form.value) ? _.toNumber(0) : '';
      if (form.type === 'checkbox') {
        form.values = [];
      }
      if (form.group && typeof form.group === 'number') {
        if (!this.formGroups[form.group] || !_.isArray(this.formGroups[form.group])) {
          this.formGroups[form.group] = [];
        }
        this.formGroups[form.group].push(form);
      } else {
        if (!this.formGroups[0] || !_.isArray(this.formGroups[0])) {
          this.formGroups[0] = [];
        }
        this.formGroups[0].push(form);
      }
    })
  }
}

export const app = ($el) => {
  return new FormControl($el);
}
