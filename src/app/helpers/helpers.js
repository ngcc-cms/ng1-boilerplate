import { ElementHelper } from './element.helper';
import { StringHelper } from './string.helper';
import { RoutingHelper } from './routing.helper';

const $ = new ElementHelper();
const _ = new StringHelper();
const $route = new RoutingHelper();

export {
    $,
    _,
    $route
}